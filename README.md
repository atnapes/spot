# Spot

An Android application to explore adjacent places. It is a showcase of android application with the latest design patterns done in a clean way. 

### Major libraries

- Kotlin
- Android Architecture Components (ViewModel, LiveData, Room)
- Dagger2
- Retrofit
- RxJava
- Moshi and Kotshi
- Glide

### Caching Strategy


        report users live location with the specified interval
        if app is showing online data relating to a location within `LOCATION_CHANGE_RESOLUTION_RATIO` 0f radius
            ignore reported location
        else
            search in cache for reported locations within the distance equal to that ratio of radius
            if found
                show from cache
            else
                fetch from remote
                    if provided
                        cache results
                        show results
                    else
                        search in cache for reported locations within the distance equal to 2 x radius
                        if provided
                            for each
                                search for venues within the 1 x radius
                                show the result
                        else
                            show empty state