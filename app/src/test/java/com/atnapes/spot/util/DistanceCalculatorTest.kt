package com.atnapes.spot.util

import org.junit.Assert.*
import org.junit.Test

class DistanceCalculatorTest {

    // sample data derived from google map
    private val taftanDamavandDistanceKm = 1_177.11
    private val damavandLat = 35.954913
    private val damavandLng = 52.109945
    private val taftanLat = 28.600413
    private val taftanLng = 61.132629
    private val taftanDamavandDistanceMi = 731.42
    private val taftanDamavandDistanceNMi = 635.15

    @Test
    fun testDistanceCalculationInMiles() {
        val comparisonDelta = .01

        val distance = distance(damavandLat, damavandLng, taftanLat, taftanLng, "M")
        assertEquals(taftanDamavandDistanceMi, distance, comparisonDelta)
    }

    @Test
    fun testDistanceCalculationInNauticalMiles() {
        val comparisonDelta = .01

        val distance = distance(damavandLat, damavandLng, taftanLat, taftanLng, "N")
        assertEquals(taftanDamavandDistanceNMi, distance, comparisonDelta)
    }

    @Test
    fun testDistanceCalculationInKilometers() {
        val comparisonDelta = .1

        val distance = distance(damavandLat, damavandLng, taftanLat, taftanLng, "K")
        assertEquals(taftanDamavandDistanceKm, distance, comparisonDelta)
    }
}