package com.atnapes.spot.di

import com.atnapes.spot.*
import com.atnapes.spot.data.remote.VenueRecommendationService
import com.atnapes.spot.util.ApplicationJsonAdapterFactory
import com.atnapes.spot.util.Memory
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@Module
open class NetworkModule {

    companion object {
        const val RETROFIT_EXPLORE = "explore"
    }

    open fun buildOkHttpClient(app: App): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(NETWORK_TIMEOUT_CONNECT, TimeUnit.SECONDS)
            .writeTimeout(NETWORK_TIMEOUT_WRITE, TimeUnit.SECONDS)
            .readTimeout(NETWORK_TIMEOUT_READ, TimeUnit.SECONDS)
            .cache(Cache(File(app.cacheDir, "OkCache"),
                Memory.calcCacheSize(app, .25f)))
            .build()

    @Provides
    @Singleton
    fun provideOkHttpClient(app: App): OkHttpClient = buildOkHttpClient(app)

    @Singleton
    @Provides
    fun provideMoshi(): Moshi = Moshi.Builder()
        .add(ApplicationJsonAdapterFactory.INSTANCE)
        .build()

    @Provides
    @Singleton
    @Named(RETROFIT_EXPLORE)
    fun provideRetrofitForExplore(okHttpClient: OkHttpClient, moshi: Moshi): Retrofit =
        Retrofit.Builder()
            .baseUrl(FOURSQUARE_HOST)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    @Singleton
    fun providePlanetService(@Named(RETROFIT_EXPLORE) retrofit: Retrofit): VenueRecommendationService =
        retrofit.create(VenueRecommendationService::class.java)
}

