package com.atnapes.spot.di

import com.atnapes.spot.ui.venuelist.VenueListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class MainModule {
    @ContributesAndroidInjector
    internal abstract fun contributeTopFragmentInjector(): VenueListFragment
}
