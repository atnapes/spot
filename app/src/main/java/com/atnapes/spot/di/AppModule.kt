package com.atnapes.spot.di

import android.app.Application
import com.atnapes.spot.App
import dagger.Binds
import dagger.Module


@Module(includes = [DatabaseModule::class, NetworkModule::class, ViewModelModule::class])
abstract class AppModule {

    @Binds
    internal abstract fun application(app: App): Application

}
