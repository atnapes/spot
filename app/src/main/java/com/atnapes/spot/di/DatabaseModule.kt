package com.atnapes.spot.di

import androidx.room.Room
import com.atnapes.spot.App
import com.atnapes.spot.CACHE_DB_NAME
import com.atnapes.spot.data.local.AppDatabase
import com.atnapes.spot.data.local.RecommendationDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(app: App): AppDatabase {
        return Room.databaseBuilder(app, AppDatabase::class.java, CACHE_DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    @Singleton
    fun provideRecommendationDao(db: AppDatabase): RecommendationDao = db.recommendationDao
}