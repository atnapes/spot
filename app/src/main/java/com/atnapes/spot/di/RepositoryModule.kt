package com.atnapes.spot.di

import com.atnapes.spot.data.repository.VenueRepository
import com.atnapes.spot.data.repository.VenueRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindSomeRepository(repository: VenueRepositoryImpl): VenueRepository
}
