package com.atnapes.spot.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.atnapes.spot.ui.venuelist.VenueViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [RepositoryModule::class])
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(VenueViewModel::class)
    abstract fun bindSomeViewModel(viewModel: VenueViewModel): ViewModel
}
