package com.atnapes.spot.di

import com.atnapes.spot.App
import com.atnapes.spot.data.local.Converters
import com.atnapes.spot.util.GlideModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            ActivityBuilder::class,
            AppModule::class
        ])
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun database(database: DatabaseModule): Builder

        fun network(network: NetworkModule): Builder

        fun build(): AppComponent
    }

    override fun inject(app: App)

    fun inject(glideModule: GlideModule)

    fun inject(converters: Converters)

}
