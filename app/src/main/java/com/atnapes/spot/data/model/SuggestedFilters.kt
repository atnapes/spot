package com.atnapes.spot.data.model


data class SuggestedFilters(
        val filters: List<Filter>,
        val header: String
)