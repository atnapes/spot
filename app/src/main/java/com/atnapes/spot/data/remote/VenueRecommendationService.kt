package com.atnapes.spot.data.remote

import com.atnapes.spot.EXPLORATION_RADIUS
import com.atnapes.spot.FOURSQUARE_CLIENT_ID
import com.atnapes.spot.FOURSQUARE_CLIENT_SECRET
import com.atnapes.spot.FOURSQUARE_VERSION
import com.atnapes.spot.data.model.FoursquareApiResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface VenueRecommendationService {

    @GET("venues/explore")
    fun getVenueRecomendations(
        @Query("client_id") clientId: String = FOURSQUARE_CLIENT_ID,
        @Query("client_secret") clientSecret: String = FOURSQUARE_CLIENT_SECRET,
        @Query("ll") latLng: String?,
        @Query("near") near: String?,
//        @Query("llAcc") latLngAccuracy: Double = 10000.0,
//        @Query("alt") altitude: Long = 0L,
//        @Query("altAcc") altitudeAccuracy: Double = 10000.0,
        @Query("radius") radius: Int = EXPLORATION_RADIUS,
        @Query("section") section: Section? = null,
        @Query("query") query: String? = null,
        @Query("limit") limit: Int,
        @Query("offset") offset: Long,
//        @Query("novelty") novelty: Novelty? = null,
//        @Query("friendVisits") friendVisits: FriendVisits? = null,
//        @Query("time") time: String = "any",
//        @Query("day") day: String = "any",
//        @Query("lastVenue") lastVenue: String? = null,
//        @Query("openNow") openNow: Boolean = false,
        @Query("sortByDistance") sortByDistance: Boolean = true,
//        @Query("price") price: String? = "1,2",
//        @Query("saved") saved: Boolean = false,
        @Query("v") v: String = FOURSQUARE_VERSION
    ) : Single<FoursquareApiResponse>
}

enum class Section {
    food,
    drinks,
    coffee,
    shops,
    arts,
    outdoors,
    sights,
    trending,
    nextVenues, //venues frequently visited after a given venue,
    topPicks
}

enum class Novelty {
    new,
    old
}

enum class FriendVisits {
    visited,
    notvisited
}