package com.atnapes.spot.data.local

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.atnapes.spot.data.model.LatLng
import com.atnapes.spot.data.model.Response
import java.io.Serializable
import java.sql.Date

@Entity(tableName = "recommendation")
data class Recommendation(
        @PrimaryKey(autoGenerate = true)
        var rowId: Long,

        val date: Date,
        val ll: LatLng,
        val response: Response,
        val offlineData: Boolean = false
): Serializable