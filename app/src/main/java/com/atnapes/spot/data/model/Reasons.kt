package com.atnapes.spot.data.model

import java.io.Serializable


data class Reasons(
        val count: Int,
        val items: List<ItemX>
): Serializable