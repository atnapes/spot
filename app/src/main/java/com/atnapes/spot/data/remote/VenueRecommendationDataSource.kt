package com.atnapes.spot.data.remote

import com.atnapes.spot.PAGING_LIMIT
import javax.inject.Inject

class VenueRecommendationDataSource @Inject constructor(private val service: VenueRecommendationService) {
    fun getVenueRecommendations(latLng: String) =
        service.getVenueRecomendations(latLng = latLng, near = null, limit = PAGING_LIMIT, offset = 0)
}
