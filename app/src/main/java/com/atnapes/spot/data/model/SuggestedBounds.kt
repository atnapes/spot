package com.atnapes.spot.data.model


data class SuggestedBounds(
        val ne: LatLng,
        val sw: LatLng
)