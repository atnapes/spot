package com.atnapes.spot.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface RecommendationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(response: Recommendation): Long

    @Query("SELECT * FROM recommendation")
    fun getAll(): Flowable<Recommendation>

    @Query("SELECT * FROM recommendation")
    fun getAllAsSingle(): Single<List<Recommendation>>
}