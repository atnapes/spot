package com.atnapes.spot.data.repository

import com.atnapes.spot.data.local.Recommendation
import com.atnapes.spot.data.local.RecommendationDao
import com.atnapes.spot.data.model.Group
import com.atnapes.spot.data.model.LatLng
import com.atnapes.spot.data.model.Response
import com.atnapes.spot.data.model.VenueItem
import com.atnapes.spot.data.remote.VenueRecommendationDataSource
import com.atnapes.spot.util.Result
import com.atnapes.spot.util.locationsAreNear
import com.atnapes.spot.util.locationsAreWithinDiameter
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.sql.Date
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class VenueRepositoryImpl @Inject constructor(
    private val dataSource: VenueRecommendationDataSource,
    private val recommendationDao: RecommendationDao
) : VenueRepository {

    override fun getRecommendedVenues(ll: LatLng): Single<Result<Recommendation>> {
        return dataSource
            .getVenueRecommendations(ll.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { Result.success(Recommendation(0, Date(java.util.Date().time), ll, it.response)) }
            .onErrorReturn { Result.failure(it) }
    }

    override fun searchCacheForNearbyLocation(ll: LatLng): Maybe<Result<Recommendation>> {
        return recommendationDao.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .filter { locationsAreNear(ll, it.ll) }
            .firstElement()
            .timeout(5, TimeUnit.SECONDS)
            .map { Result.success(it) }
            .onErrorReturn { Result.failure(it) }
    }

    override fun searchCacheForNearbyVenues(ll: LatLng): Single<Result<Recommendation>> {
        return recommendationDao.getAllAsSingle()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flattenAsFlowable { it }
            .filter {
                locationsAreWithinDiameter(ll, it.ll)
            }
            .flatMapIterable { it.response.groups }
            .flatMapIterable { it.items }
            .filter {
                locationsAreNear(ll, LatLng(it.venue.location.lat, it.venue.location.lng))
            }
            .distinct()
            .collectInto(mutableListOf<VenueItem>()) { t1, t2 -> t1.add(t2) }
            .map {
                buildCacheResults(it)
            }
            .onErrorReturn { Result.failure(it) }
    }

    private fun buildCacheResults(list: List<VenueItem>): Result<Recommendation> {
        val group = Group(list, "Recommended", "Offline Recommendations")
        val location = "Offline Data"
        val response = Response(listOf(group), location, location, location, list.size)
        val ll = LatLng(list[0].venue.location.lat, list[0].venue.location.lng)
        val recommendation = Recommendation(0, Date(java.util.Date().time), ll, response, true)
        return Result.success(recommendation)
    }

    override fun save(recommendation: Recommendation) {
        Observable.fromCallable {
            recommendationDao.save(recommendation)
        }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }

}
