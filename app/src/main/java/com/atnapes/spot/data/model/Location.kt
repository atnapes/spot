package com.atnapes.spot.data.model

import java.io.Serializable


data class Location(
        val address: String,
        val cc: String,
        val city: String,
        val country: String,
        val crossStreet: String,
        val distance: Int,
        val formattedAddress: List<String>,
        val labeledLatLngs: List<LabeledLatLng>,
        val lat: Double,
        val lng: Double,
        val state: String
): Serializable