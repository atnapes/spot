package com.atnapes.spot.data.model


data class Meta(
        val code: Int,
        val requestId: String
)