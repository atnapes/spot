package com.atnapes.spot.data.model

import java.io.Serializable


data class ItemX(
        val reasonName: String,
        val summary: String,
        val type: String
): Serializable