package com.atnapes.spot.data.model

import java.io.Serializable


data class Icon(
        val prefix: String,
        val suffix: String
): Serializable