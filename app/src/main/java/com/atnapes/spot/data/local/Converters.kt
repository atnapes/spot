package com.atnapes.spot.data.local

import androidx.room.TypeConverter
import com.atnapes.spot.App
import com.atnapes.spot.data.model.LatLng
import com.atnapes.spot.data.model.Response
import com.atnapes.spot.util.AppInjection
import com.squareup.moshi.Moshi
import java.sql.Date
import javax.inject.Inject

class Converters {

    @Inject lateinit var moshi: Moshi

    init {
        // We neither control the object creation, nor get access to the object instance afterwards.
        // So I didn't find a cleaner way to inject this class
        AppInjection.of(App.instance).inject(this)
    }

    @TypeConverter
    fun serializeResponse(response: Response?): String? {
        response ?: return null
        val adapter = moshi.adapter(Response::class.java)
        return adapter.toJson(response)
    }

    @TypeConverter
    fun deserializeResponse(json: String?): Response? {
        json ?: return null
        val adapter = moshi.adapter(Response::class.java)
        return adapter.fromJson(json)
    }

    @TypeConverter
    fun serializeLatLng(latLng: LatLng?): String? {
        latLng ?: return null
        val adapter = moshi.adapter(LatLng::class.java)
        return adapter.toJson(latLng)
    }

    @TypeConverter
    fun deserializeLatLng(json: String?): LatLng? {
        json ?: return null
        val adapter = moshi.adapter(LatLng::class.java)
        return adapter.fromJson(json)
    }

    @TypeConverter
    fun serializeDate(date: Date?): Long? {
        date ?: return null
        return date.time
    }

    @TypeConverter
    fun deserializeDate(time: Long?): Date? {
        time ?: return null
        return Date(time)
    }
}