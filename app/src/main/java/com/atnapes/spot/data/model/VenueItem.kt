package com.atnapes.spot.data.model

import java.io.Serializable


data class VenueItem(
        val reasons: Reasons,
        val referralId: String,
        val venue: Venue
): Serializable {
        override fun equals(other: Any?) = other is VenueItem && other.venue.id == venue.id
        override fun hashCode() = venue.id.hashCode()
}
