package com.atnapes.spot.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.atnapes.spot.CACHE_DB_SCHEMA_VERSION

@Database(
        entities = [Recommendation::class],
        version = CACHE_DB_SCHEMA_VERSION,
        exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract val recommendationDao: RecommendationDao

}