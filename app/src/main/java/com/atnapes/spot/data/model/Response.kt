package com.atnapes.spot.data.model

import java.io.Serializable


data class Response(
        val groups: List<Group>,
        val headerFullLocation: String,
        val headerLocation: String,
        val headerLocationGranularity: String,
        val totalResults: Int

        // unneeded data
//        val query: String,
//        val suggestedBounds: SuggestedBounds,
//        val suggestedFilters: SuggestedFilters,
//        val suggestedRadius: Int,
): Serializable