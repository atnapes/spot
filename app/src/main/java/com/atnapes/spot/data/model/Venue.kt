package com.atnapes.spot.data.model

import java.io.Serializable


data class Venue(
        val categories: List<Category>,
        val id: String,
        val location: Location,
        val name: String,
        val photos: Photos
): Serializable