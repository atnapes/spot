package com.atnapes.spot.data.model


data class LatLng(
        val lat: Double,
        val lng: Double
) {
        override fun toString() = "$lat,$lng"
}