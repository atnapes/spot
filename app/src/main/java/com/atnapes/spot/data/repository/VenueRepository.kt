package com.atnapes.spot.data.repository

import com.atnapes.spot.data.local.Recommendation
import com.atnapes.spot.data.model.LatLng
import com.atnapes.spot.util.Result
import io.reactivex.Maybe
import io.reactivex.Single

interface VenueRepository {

    fun getRecommendedVenues(ll: LatLng): Single<Result<Recommendation>>

    fun searchCacheForNearbyLocation(ll: LatLng): Maybe<Result<Recommendation>>

    fun searchCacheForNearbyVenues(ll: LatLng): Single<Result<Recommendation>>

    fun save(recommendation: Recommendation)
}