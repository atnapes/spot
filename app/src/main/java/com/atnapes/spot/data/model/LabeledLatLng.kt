package com.atnapes.spot.data.model

import java.io.Serializable


data class LabeledLatLng(
        val label: String,
        val lat: String,
        val lng: String
): Serializable