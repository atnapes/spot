package com.atnapes.spot.data.model


data class Filter(
        val key: String,
        val name: String
)