package com.atnapes.spot.data.model

import java.io.Serializable


data class Photos(
        val count: Int,
        val groups: List<Any>
): Serializable