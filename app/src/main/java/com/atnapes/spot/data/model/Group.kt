package com.atnapes.spot.data.model

import java.io.Serializable


data class Group(
    val items: List<VenueItem>,
    val name: String,
    val type: String
): Serializable