package com.atnapes.spot.data.model


data class FoursquareApiResponse(
        val meta: Meta,
        val response: Response
)