package com.atnapes.spot.util

import com.atnapes.spot.EXPLORATION_RADIUS
import com.atnapes.spot.LOCATION_CHANGE_RESOLUTION_RATIO
import com.atnapes.spot.data.model.LatLng
import java.lang.Math.toDegrees
import java.lang.Math.toRadians
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.sin

/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource (TM) products   :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles (default)                         :*/
/*::                  'K' is kilometers                                      :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at https://www.geodatasource.com                         :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: https://www.geodatasource.com                       :*/
/*::                                                                         :*/
/*::           GeoDataSource.com (C) All Rights Reserved 2018                :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

fun distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double, unit: String): Double {
    if (lat1 == lat2 && lon1 == lon2) {
        return 0.0
    } else {
        val theta = lon1 - lon2
        var dist =
            sin(toRadians(lat1)) * sin(toRadians(lat2)) + cos(toRadians(lat1)) * cos(toRadians(lat2)) * cos(toRadians(theta))
        dist = acos(dist)
        dist = toDegrees(dist)
        dist *= 60.0 * 1.1515
        if ("K" == unit) {
            dist *= 1.609344
        } else if ("N" == unit) {
            dist *= 0.8684
        }
        return dist
    }
}

fun locationsAreWithinDistance(l1: LatLng, l2: LatLng, distance: Double): Boolean =
    distance(l1.lat, l1.lng, l2.lat, l2.lng, "K") < distance

fun locationsAreNear(l1: LatLng, l2: LatLng): Boolean =
    locationsAreWithinDistance(l1, l2, EXPLORATION_RADIUS * LOCATION_CHANGE_RESOLUTION_RATIO)

fun locationsAreWithinDiameter(l1: LatLng, l2: LatLng): Boolean =
    locationsAreWithinDistance(l1, l2, EXPLORATION_RADIUS * 2.0)