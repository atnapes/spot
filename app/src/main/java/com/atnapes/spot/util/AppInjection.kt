package com.atnapes.spot.util

import android.content.Context
import com.atnapes.spot.App
import com.atnapes.spot.di.AppComponent

object AppInjection {

    fun of(context: Context?): AppComponent {
        return (context as App).applicationInjector() as AppComponent
    }
}