package com.atnapes.spot

import android.content.Context
import com.atnapes.spot.data.local.Converters
import com.atnapes.spot.di.DaggerAppComponent
import com.atnapes.spot.di.DatabaseModule
import com.atnapes.spot.di.NetworkModule
import com.atnapes.spot.util.AppInjection
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import timber.log.Timber


open class App : DaggerApplication() {

    companion object {
        lateinit var instance: App
    }

    private lateinit var androidInjector: AndroidInjector<out DaggerApplication>

    override fun onCreate() {
        super.onCreate()

        instance = this

        setupTimber()

        setupThreeTen()
    }

    private fun setupThreeTen() {
        AndroidThreeTen.init(this)
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)

        androidInjector = DaggerAppComponent.builder()
            .network(networkModule())
            .database(databaseModule())
            .application(this)
            .build()
    }

    public override fun applicationInjector(): AndroidInjector<out DaggerApplication> = androidInjector

    protected open fun networkModule(): NetworkModule = NetworkModule()

    protected open fun databaseModule(): DatabaseModule = DatabaseModule()

}
