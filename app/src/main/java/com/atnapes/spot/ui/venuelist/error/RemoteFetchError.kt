package com.atnapes.spot.ui.venuelist.error

class RemoteFetchError(e: Throwable) : Throwable(e)