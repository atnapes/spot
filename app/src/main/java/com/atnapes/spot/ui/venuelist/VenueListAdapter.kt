package com.atnapes.spot.ui.venuelist

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.atnapes.spot.data.model.VenueItem
import com.atnapes.spot.ui.listeners.OnItemClickListener


class VenueListAdapter(private val listener: OnItemClickListener<VenueItem>)
    : ListAdapter<VenueItem, VenueViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        VenueViewHolder.getInstance(parent, listener)

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        holder.bindToVenue(getItem(position))
    }

    object DiffCallback: DiffUtil.ItemCallback<VenueItem>() {
        override fun areItemsTheSame(oldVenueItem: VenueItem, newVenueItem: VenueItem): Boolean {
            return oldVenueItem.equals(newVenueItem)
        }

        override fun areContentsTheSame(oldVenueItem: VenueItem, newVenueItem: VenueItem): Boolean {
            return oldVenueItem.equals(newVenueItem)
        }

    }
}
