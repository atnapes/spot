package com.atnapes.spot.ui.listeners

interface OnItemClickListener<T> {
    fun onItemClicked(item: T)
}