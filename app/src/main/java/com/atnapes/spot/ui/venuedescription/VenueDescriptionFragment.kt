package com.atnapes.spot.ui.venuedescription

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.atnapes.spot.R
import com.atnapes.spot.data.model.VenueItem
import com.atnapes.spot.ui.widget.LabeledTextView
import kotlinx.android.synthetic.main.fragment_venuedescription.*

class VenueDescriptionFragment : Fragment() {

    companion object {
        private const val KEY_VENUE_ITEM = "venue_item"
        
        fun newInstance(item: VenueItem): VenueDescriptionFragment {
            val instance = VenueDescriptionFragment()
            val args = Bundle()
            args.putSerializable(KEY_VENUE_ITEM, item)
            instance.arguments = args
            return instance
        }
    }

    private lateinit var item: VenueItem

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val arg = arguments?.get(KEY_VENUE_ITEM) as VenueItem?
        arg ?: throw IllegalStateException("Missing venue item argument for VenueDescriptionFragment, " +
                "Perhaps you ain't instantiating it with provided newInstance() method")
        item = arg
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_venuedescription, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        addLabeledText("Name:", item.venue.name)
        addLabeledText("Address", item.venue.location.formattedAddress.joinToString("\n"))
        addLabeledText("Referral id", item.referralId)
        addLabeledText("Latitude", item.venue.location.lat.toString())
        addLabeledText("Longitude", item.venue.location.lng.toString())

        item.venue.categories.forEachIndexed { i, cat ->
            addLabeledText("Category #$i short name:", cat.shortName)
            addLabeledText("Category #$i name:", cat.name)
            addLabeledText("Category #$i plural name:", cat.pluralName)
            addLabeledText("Is primary category:", cat.primary.toString())
        }

        item.reasons.items.forEachIndexed { i, reason ->
            addLabeledText("Reason #$i name:", reason.reasonName)
            addLabeledText("Reason #$i summary:", reason.summary)
            addLabeledText("Reason #$i type", reason.type)
        }
    }

    private fun addLabeledText(label: String, value: String) {
        val labeledTextView = LabeledTextView(context!!)
        labeledTextView.setLabel(label)
        labeledTextView.setValue(value)
        val params = LinearLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT)
        val verticalMargin = resources.getDimensionPixelSize(R.dimen.venuedescriptin_item_verticalmargin)
        params.topMargin = verticalMargin
        params.bottomMargin = verticalMargin
        container.addView(labeledTextView, params)
    }
}
