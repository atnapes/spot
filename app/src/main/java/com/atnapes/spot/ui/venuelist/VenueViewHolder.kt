package com.atnapes.spot.ui.venuelist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.atnapes.spot.R
import com.atnapes.spot.data.model.VenueItem
import com.atnapes.spot.ui.listeners.OnItemClickListener
import com.atnapes.spot.ui.widget.LabeledTextView
import com.atnapes.spot.util.distance

class VenueViewHolder private constructor(itemView: View,
                                          private val listener: OnItemClickListener<VenueItem>)
    : RecyclerView.ViewHolder(itemView) {

    companion object {
        fun getInstance(parent: ViewGroup, listener: OnItemClickListener<VenueItem>): VenueViewHolder {
            val inflater = LayoutInflater.from(parent.context)
            val itemView = inflater.inflate(R.layout.item_venue, parent, false)
            return VenueViewHolder(itemView, listener)
        }
    }

    private lateinit var item: VenueItem

    init {
        itemView.setOnClickListener {
            if (::item.isInitialized) {
                listener.onItemClicked(item)
            }
        }
    }

    fun bindToVenue(venueItem: VenueItem) {
        item = venueItem

        val textViewDistance = itemView.findViewById<TextView>(R.id.textViewDistance)
        val textViewName = itemView.findViewById<LabeledTextView>(R.id.textViewName)
        val textViewAddress = itemView.findViewById<LabeledTextView>(R.id.textViewAddress)

        textViewName.setValue(venueItem.venue.name)

        val lastLocation = VenueListFragment.lastReportedLocation
        if (null != lastLocation) {
            val location = item.venue.location
            var distance =
                distance(lastLocation.latitude, lastLocation.longitude, location.lat, location.lng, "K")
            distance *= 1000
            textViewDistance.text =
                itemView.context.getString(R.string.venueitem_textview_distance_format, distance.toInt())
        }

        textViewAddress.setValue(item.venue.location.formattedAddress.joinToString("\n"))
    }

}
