package com.atnapes.spot.ui.venuelist

import android.annotation.SuppressLint
import android.location.Location
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.atnapes.spot.LOCATION_CHANGE_RESOLUTION_RATIO
import com.atnapes.spot.data.local.Recommendation
import com.atnapes.spot.data.model.LatLng
import com.atnapes.spot.data.repository.VenueRepository
import com.atnapes.spot.ui.BaseViewModel
import com.atnapes.spot.ui.venuelist.error.RemoteFetchError
import com.atnapes.spot.ui.venuelist.error.VenueSearchError
import com.atnapes.spot.util.Result
import com.atnapes.spot.util.ext.toLiveData
import com.atnapes.spot.util.locationsAreNear
import io.reactivex.BackpressureStrategy
import io.reactivex.subjects.PublishSubject
import org.threeten.bp.LocalTime
import org.threeten.bp.format.DateTimeFormatter
import timber.log.Timber
import javax.inject.Inject

class VenueViewModel @Inject constructor(
    private val venueRepository: VenueRepository
) : BaseViewModel(), LifecycleObserver {

    companion object {
        const val FORMAT_LOG_TIME = "HH:mm:ss"
    }

    private var _liveData = MutableLiveData<Result<Recommendation>>()
    val liveData = MediatorLiveData<Result<Recommendation>>()

    private var subject: PublishSubject<String> = PublishSubject.create()
    val logLiveData: LiveData<String> = subject.toFlowable(BackpressureStrategy.BUFFER).toLiveData()

    val logList = mutableListOf<String>()

    @SuppressLint("CheckResult")
    fun reportLocation(location: Location) {
        val ll = LatLng(location.latitude, location.longitude)

        log("Reported location: $ll")

        val value: Result<Recommendation>? = _liveData.value
        if (value != null) {
            when (value) {
                is Result.Success -> {
                    if (!value.data.offlineData
                        && locationsAreNear(ll, value.data.ll)) {
                        log("Reported location is near last showing data. Ignoring.")
                        return
                    } else {
                        log("Location has changed")
                    }
                }
                is Result.Failure -> {
                    log("There is a failed value in live data with message: ${value.message}")
                }
            }

        } else {
            log("LiveData is empty")
        }

        searchCacheForLocation(ll)
    }

    private var nearbyCacheSearchInProgress = false
    private var nearVenuesCacheSeachInProgress = false
    private var remoteFetchInProgress = false

    private fun searchCacheForLocation(ll: LatLng) {

        if (nearbyCacheSearchInProgress) return
        nearbyCacheSearchInProgress = true

        log("Searching in cache for location: $ll")

        val subscribe = venueRepository.searchCacheForNearbyLocation(ll)
            .subscribe {
                nearbyCacheSearchInProgress = false
                when (it) {
                    is Result.Success -> {
                        log("Found data for a location within $LOCATION_CHANGE_RESOLUTION_RATIO" +
                                " * radius in cache, skipping remote call")
                        handToView(it)
                    }
                    is Result.Failure -> {
                        log("Failed to search in cache with message: ${it.message}")
                        requestFromRemote(ll)
                    }
                }
            }

        addToDisposable(subscribe)
    }

    private fun requestFromRemote(ll: LatLng) {
        if(remoteFetchInProgress) return
        remoteFetchInProgress = true

        log("Fetching from remote for location: $ll")

        val subscribe = venueRepository.getRecommendedVenues(ll)
            .subscribe(
                {
                    when (it) {
                        is Result.Success -> {
                            remoteFetchInProgress = false
                            log("Successfully fetched from remote. showing and caching...")
                            saveToCache(it.data)
                            handToView(it)
                        }
                        is Result.Failure -> {
                            remoteFetchInProgress = false
                            log("Failed to fetch from remote with message: ${it.message}")
                            handToView(Result.failure(RemoteFetchError(it.message)))
                            searchCacheForNearVenues(ll)
                        }
                    }
                },
                {
                    log("Failed to fetch from remote with message: ${it.message}")
                    handToView(Result.failure(RemoteFetchError(it)))
                    searchCacheForNearVenues(ll)
                }
            )

        addToDisposable(subscribe)
    }

    private fun searchCacheForNearVenues(ll: LatLng) {

        if (nearVenuesCacheSeachInProgress) return
        nearVenuesCacheSeachInProgress = true

        log("Searching for near venues in cache for location $ll")

        val subscribe = venueRepository.searchCacheForNearbyVenues(ll)
            .subscribe(
                {
                    nearVenuesCacheSeachInProgress = false
                    when (it) {
                        is  Result.Success-> {
                            log("Successfully find ${it.data.response.totalResults} near venues")
                            handToView(it)
                        }
                        is Result.Failure -> {
                            log("Failed to find near venues with message: ${it.message.message}")
                        }
                    }
                },
                {
                    nearVenuesCacheSeachInProgress = false
                    handToView(Result.failure(VenueSearchError(it)))
                    log("Failed to find near venues with message: ${it.message}")
                }
            )

        addToDisposable(subscribe)
    }

    private fun log(message: String) {
        Timber.d(message)

        val now = LocalTime.now()
        val time = now.format(DateTimeFormatter.ofPattern(FORMAT_LOG_TIME))
        val logMessage = "$time $message"

        logList.add(logMessage)
        subject.onNext(logMessage)
    }

    private fun saveToCache(recommendation: Recommendation) {
        venueRepository.save(recommendation)
    }

    private fun handToView(result: Result<Recommendation>) {
        _liveData.value = result
        liveData.removeSource(_liveData)
        liveData.addSource(_liveData) { liveData.value = it }
    }
}