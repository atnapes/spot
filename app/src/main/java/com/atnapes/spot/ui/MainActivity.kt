package com.atnapes.spot.ui

import android.Manifest
import android.animation.Animator
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import com.atnapes.spot.R
import com.atnapes.spot.data.model.VenueItem
import com.atnapes.spot.ui.listeners.OnItemClickListener
import com.atnapes.spot.ui.venuedescription.VenueDescriptionFragment
import com.atnapes.spot.ui.venuelist.VenueListFragment
import com.atnapes.spot.util.ext.makeInvisible
import com.atnapes.spot.util.ext.makeVisible
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity(), OnItemClickListener<VenueItem> {

    private val locationPermission: String = Manifest.permission.ACCESS_FINE_LOCATION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (hasLocationPermission()) {
            addVenueListFragment()
        } else {
            showPermissionWidgets()
        }
    }

    private fun addVenueListFragment() {
        showContainer()

        val venueListFragment = supportFragmentManager
            .findFragmentByTag(TAG_VENUE_LIST_FRAGMENT)

        if (null == venueListFragment) {
            val fragment = VenueListFragment()
            supportFragmentInjector().inject(fragment)
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container, fragment, TAG_VENUE_LIST_FRAGMENT)
                .commit()
        }
    }

    override fun onItemClicked(item: VenueItem) = addVenueDescriptionFragment(item)

    private fun addVenueDescriptionFragment(venueItem: VenueItem) {
        showContainer()

        val venueListFragment = supportFragmentManager
            .findFragmentByTag(TAG_VENUE_DESCRIPTION_FRAGMENT)

        if (null == venueListFragment) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container,
                    VenueDescriptionFragment.newInstance(venueItem),
                    TAG_VENUE_DESCRIPTION_FRAGMENT)
                .addToBackStack(null)
                .commit()
        }
    }

    private fun hasLocationPermission(): Boolean {
        val locationPermissionCheck =
            ContextCompat.checkSelfPermission(this, locationPermission)
        return locationPermissionCheck == PermissionChecker.PERMISSION_GRANTED
    }

    fun requestPermission(view: View) =
        requestPermissions(arrayOf(locationPermission), REQ_CODE_LOCATION_PERMISSION)

    private fun showPermissionWidgets() {
        viewPermissionRequest.makeVisible()
        container.makeInvisible()
    }

    private fun showContainer() {
        viewPermissionRequest
            .animate()
            .alpha(0f)
            .setDuration(resources.getInteger(R.integer.default_animation_duration).toLong())
            .setListener(object : SimpleAnimatorListener() {
                override fun onAnimationEnd(animation: Animator?) {
                    viewPermissionRequest.makeInvisible()
                }
            })
            .startDelay

        container.makeVisible()
    }

    open class SimpleAnimatorListener : Animator.AnimatorListener {
        override fun onAnimationEnd(animation: Animator?) {}
        override fun onAnimationCancel(animation: Animator?) {}
        override fun onAnimationStart(animation: Animator?) {}
        override fun onAnimationRepeat(animation: Animator?) {}
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQ_CODE_LOCATION_PERMISSION) {
            if (grantResults[0] == PermissionChecker.PERMISSION_GRANTED) {
                addVenueListFragment()
            } else {
                showPermissionWidgets()
            }
        }
    }

    companion object {
        private const val REQ_CODE_LOCATION_PERMISSION = 32123

        private const val TAG_VENUE_LIST_FRAGMENT = "Venue list fragment"
        private const val TAG_VENUE_DESCRIPTION_FRAGMENT = "Venue description fragment"
    }

}
