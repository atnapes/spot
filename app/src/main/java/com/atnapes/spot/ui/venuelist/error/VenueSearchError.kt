package com.atnapes.spot.ui.venuelist.error

class VenueSearchError(e: Throwable) : Throwable(e)