package com.atnapes.spot.ui.venuelist

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.atnapes.spot.R
import com.atnapes.spot.UPDATE_INTERVAL
import com.atnapes.spot.data.model.VenueItem
import com.atnapes.spot.ui.listeners.OnItemClickListener
import com.atnapes.spot.util.Result
import com.atnapes.spot.util.ext.makeGone
import com.atnapes.spot.util.ext.makeVisible
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_venuelist.*
import timber.log.Timber
import javax.inject.Inject

/**
 *
 * A fragment that gets current location and shows nearby venues to the user.
 * Because host activity has handled permission request and google api availability, in this
 * fragment we assume we have location permission granted.
 *
 */
class VenueListFragment: Fragment() {

    companion object {
        private const val REQUEST_CHECK_SETTINGS = 23422
        var lastReportedLocation: Location? = null
        const val KEY_LIST_STATE = "3233"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: VenueViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VenueViewModel::class.java)
    }

    private val locationRequest =
        LocationRequest.create().apply {
            interval = UPDATE_INTERVAL
            fastestInterval = UPDATE_INTERVAL
            priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        }

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var requestingLocationUpdates: Boolean = false

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_venuelist, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!hasLocationPermission()) {
            throw IllegalStateException("Location permission is not provided")
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)

        createLocationRequest()

        setupLogView()

        listState = savedInstanceState?.getParcelable<Parcelable>(KEY_LIST_STATE)
    }

    private var listState: Parcelable? = null

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        list.layoutManager?.onSaveInstanceState()?.let {
            listState = it
            outState.putParcelable(KEY_LIST_STATE, it)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setupLogView() {
        if (!viewModel.logList.isNullOrEmpty()) {
            textViewLog.text = viewModel.logList.joinToString("\n")
        }
        viewModel.logLiveData.observe(this, Observer {
            textViewLog.text = "${textViewLog.text}\n$it"
            scrollViewLog.post { scrollViewLog.fullScroll(View.FOCUS_DOWN) }
        })

        imageViewLogIcon.setOnClickListener {
            if (scrollViewLog.visibility == View.VISIBLE) {
                scrollViewLog.makeGone()
            } else {
                scrollViewLog.makeVisible()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun hasLocationPermission(): Boolean {
        val locationPermissionCheck =
            ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION)
        return locationPermissionCheck == PermissionChecker.PERMISSION_GRANTED
    }

    private fun createLocationRequest() {
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        val client: SettingsClient = LocationServices.getSettingsClient(activity!!)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { locationSettingsResponse ->
            requestingLocationUpdates = true

            startLocationUpdates()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                try {
                    exception.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == Activity.RESULT_OK) {
            startLocationUpdates()
        }
    }

    @SuppressLint("MissingPermission")
    fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)

        setupUi()
    }

    private val locationCallback =  object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            super.onLocationResult(locationResult)
            locationResult ?: return
            locationResult.locations.forEach { postLocation(it) }
        }
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun postLocation(location: Location) {
        lastReportedLocation = location

        viewModel.reportLocation(location)
    }

    private fun setupUi() {
        list.layoutManager = LinearLayoutManager(context)
        listState?.let { list.layoutManager?.onRestoreInstanceState(listState) }

        val adapter = VenueListAdapter(activity as OnItemClickListener<VenueItem>)
        list.adapter = adapter

        viewModel.liveData.observe(this, Observer {
            val message = "found ${it::class.java.name} in fragment live data"
            when (it) {
                is Result.Loading -> {
                    Toast.makeText(context, "loading", Toast.LENGTH_SHORT).show()
                }
                is Result.Success -> {
                    textViewToolbarTitle.text = it.data.response.headerFullLocation
                    adapter.submitList(it.data.response.groups[0].items)
                    message + " with ${it.data.response.totalResults} results"
                }
                is Result.Failure -> {
                    Toast.makeText(context, it.message.message, Toast.LENGTH_SHORT).show()
                }
            }
            Timber.d(message)
        })
    }

}
