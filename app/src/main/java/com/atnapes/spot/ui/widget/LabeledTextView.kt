package com.atnapes.spot.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import com.atnapes.spot.R

class LabeledTextView: LinearLayoutCompat {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int):
            super(context, attrs, defStyleAttr) {
        initialize(context, attrs, defStyleAttr)
    }

    private var labelTextView: TextView? = null
    private var valueTextView: TextView? = null

    private fun initialize(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        LayoutInflater.from(context).inflate(R.layout.view_labeledtext, this, true)

        labelTextView = findViewById(R.id.text_label)
        valueTextView = findViewById(R.id.text_value)

        val a = context.obtainStyledAttributes(
            attrs, R.styleable.LabeledTextView,
            defStyleAttr, 0
        )

        if (a.hasValue(R.styleable.LabeledTextView_label)) {
            setLabel(a.getString(R.styleable.LabeledTextView_label))
        }

        if (a.hasValue(R.styleable.LabeledTextView_value)) {
            setValue(a.getString(R.styleable.LabeledTextView_value))
        }

        if (a.hasValue(R.styleable.LabeledTextView_smallTypeface)
            && a.getBoolean(R.styleable.LabeledTextView_smallTypeface, false)) {
            setSmall()
        }

        a.recycle()
    }

    fun setLabel(label: CharSequence?) {
        labelTextView!!.text = label
    }

    fun setValue(value: CharSequence?) {
        valueTextView!!.text = value
    }

    fun setTextSize(unit: Int, size: Float) {
        labelTextView!!.setTextSize(unit, size)
        valueTextView!!.setTextSize(unit, size)
    }

    fun setTextSize(size: Float) {
        labelTextView!!.textSize = size
        valueTextView!!.textSize = size
    }

    fun setSmall() {
        labelTextView!!.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16f)
        valueTextView!!.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16f)
    }
}
